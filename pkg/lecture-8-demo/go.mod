module gitlab.com/siriusfreak/lecture-8-demo/pkg/lecture-8-demo

go 1.15

require (
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.5.0
	google.golang.org/genproto v0.0.0-20210617175327-b9e0b3197ced
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)
