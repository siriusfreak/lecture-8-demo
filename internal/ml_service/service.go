package ml_service

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"

	"github.com/Shopify/sarama"
	"gitlab.com/siriusfreak/lecture-8-demo/common"
)


type Consumer struct {}

func (consumer *Consumer) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (consumer *Consumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (consumer *Consumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for message := range claim.Messages() {
		messageReceived(message)
		session.MarkMessage(message, "")
	}

	return nil
}


func subscribe(ctx context.Context, topic string, consumerGroup sarama.ConsumerGroup) error {
	consumer := Consumer{}

	go func() {
		for {
			if err := consumerGroup.Consume(ctx, []string{topic}, &consumer); err != nil {
				fmt.Printf("Error from consumer: %v", err)
			}
			if ctx.Err() != nil {
				return
			}
		}
	}()


	return nil
}

func messageReceived(message *sarama.ConsumerMessage) {
	fmt.Printf("Analyzing message: %s\n", string(message.Value))
	var msg common.TextMessage
	err := json.Unmarshal(message.Value, &msg)
	if err != nil {
		fmt.Printf("Error unmarshalling message: %s\n", err)
	}

	result := rand.Int() % 2

	resp, err := http.PostForm(msg.CallbackUrl,  url.Values{"id": {strconv.Itoa(int(msg.ID))}, "result": {strconv.Itoa(int(result))}})

	if err != nil {
		fmt.Printf("Error call callback: %v\n", err)
	} else if resp.StatusCode != 200 {
		fmt.Printf("Return code not 200: %d\n", resp.StatusCode)
	}
}

type MLService interface {
	StartConsuming() error
}

type Service struct {

}

func InitMLService() *Service {
	return &Service{}
}

var brokers = []string{"127.0.0.1:9094"}

func (s *Service) StartConsuming(ctx context.Context) error {
	version, err := sarama.ParseKafkaVersion("2.1.1")
	if err != nil {
		return err
	}

	config := sarama.NewConfig()

	config.Version = version
	config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRoundRobin
	config.Consumer.Offsets.Initial = sarama.OffsetOldest



	consumerGroup, err := sarama.NewConsumerGroup(brokers, "text", config)

	if err != nil {
		return err
	}

	return subscribe(ctx,"text",  consumerGroup)
}